function launch_chat() {
        if ($_GET('salon') == null) {
            salon_ok = false;
            throw new Error("Aucun salon selectionné, veuillez le préciser dans l'URL");
        } else {
            salon = $_GET('salon');
        }
        // ask user for name with popup prompt    
        var name = prompt("Enter your chat name:", "");
        
        // default name is 'Guest'
    	if (!name || name === ' ') {
    	   name = "Guest";	
    	}
    	
    	// strip tags
    	name = name.replace(/(<([^>]+)>)/ig,"");
    	
    	// display name on page
    	$("#name-area").html("You are: <span>" + name + "</span>");
    	
    	// kick off chat
        var chat =  new Chat();
    	$(function() {
    	
    		 chat.getState(); 
    		 
    		 // watch textarea for key presses
             $("#sendie").keydown(function(event) {  
             
                 var key = event.which;  
           
                 //all keys including return.  
                 if (key >= 33) {
                   
                     var maxLength = $(this).attr("maxlength");  
                     var length = this.value.length;  
                     
                     // don't allow new content if length is maxed out
                     if (length >= maxLength) {  
                         event.preventDefault();  
                     }  
                  }  
    		 																																																});
    		 // watch textarea for release of key press
    		 $('#sendie').keyup(function(e) {	
    		 					 
    			  if (e.keyCode == 13) { 
    			  
                    var text = $(this).val();
    				var maxLength = $(this).attr("maxlength");  
                    var length = text.length; 
                     
                    // send 
                    if (length <= maxLength + 1) { 
                     
    			        chat.send(text, name);	
    			        $(this).val("");
    			        
                    } else {
                    
    					$(this).val(text.substring(0, maxLength));
    					
    				}	
    				
    				
    			  }
             });
            
    	});
        
        return chat;
}

function launch_stream(url) {
        url ="https://pilote.u-ga.fr/live-adapt/"+url+".m3u8";
        console.log(url);
        if(Hls.isSupported()) {
          var video = document.getElementById('video');
          var hls = new Hls();
          hls.loadSource(url);
          hls.attachMedia(video);
          hls.on(Hls.Events.MANIFEST_PARSED,function() {
            video.play();
        });
       }
       // hls.js is not supported on platforms that do not have Media Source Extensions (MSE) enabled.
       // When the browser has built-in HLS support (check using `canPlayType`), we can provide an HLS manifest (i.e. .m3u8 URL) directly to the video element throught the `src` property.
       // This is using the built-in support of the plain video element, without using hls.js.
        else if (video.canPlayType('application/vnd.apple.mpegurl')) {
          video.src = url;
          video.addEventListener('canplay',function() {
            video.play();
          });
        }
}

function launch_index() {
                $(document).ready(function () {
                $("#new_salon_href").click(function () {
                   $("#dialog_new_index").show();
                   $("#fancy_new_index").show();
                });
                
                $("#dialog_new_index").click(function () {
                   $("#fancy_new_index").hide();
                   $("#dialog_new_index").hide();
                });
                
                $("#submit_button").click(function () {
                    $.ajax({
                        type: "POST",
                        url: "salons.php",
                        data: {  
                            'name': $("#input_name").val(),
                            'key' : $("#input_key").val(),
                            'mode':"write"
                        },
                        dataType: "json",

                        success: function(data){
                            alert(data);
                            document.location.reload(true);
                        },
                    });
                });
            }); 
                $.ajax({
                    type: "POST",
                    url: "salons.php",
                    data: {  
                        'salon': -99,
                        'mode':"read"
                    },
                    dataType: "json",

                    success: function(data){
                       var saloons = data['salon'];
                       if (saloons.id !== undefined) {
                           saloons = [saloons];
                       }
                       
                       if (saloons.length > 0) {
                           
                           var html= "<div id='in_list'>\n\
                                    <h3>Liste des salons disponibles</h3>";
                           html += "<table id='table_salon'>";

                           for (let i = 0; i < saloons.length; i++) {
                               html += "<tr>\n\
                                    <td>"+
                                                "<a href ='live.php?salon="+saloons[i].key+"'>"+saloons[i].id+'</a>'
                                         +
                                    "</td>\n\
                                </tr>";
                           }
                           html += "</table>\n\
                                   </div>";
                                    
                           $( "#list_salon" ).html(html);
                       }
                    },
                });
}