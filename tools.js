        /**
         * Take url get param
         * @param {type} param
         * @returns {vars|$_GET.vars}
         */
        function $_GET(param) {
                var vars = {};
                window.location.href.replace( location.hash, '' ).replace( /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
                    function( m, key, value ) { // callback
                        vars[key] = value !== undefined ? value : '';
                    }
                );

            if ( param ) {
                return vars[param] ? vars[param] : null;	
            }
            return vars;
        }
        
        var url;
        
        function get_salon_url() {
            $.ajax({
                type: "POST",
		url: "salons.php",
		data: {  
                    'salon': salon,
                    'mode':'read'
                },
		dataType: "json",
			
		success: function(data){
                    url = data["0"];  
                },
	    });
        }