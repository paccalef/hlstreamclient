<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Hls Stream Client</title>
    
    <link rel="stylesheet" href="style.css" type="text/css" />
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="chat.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
    <script type="text/javascript" src="launchers.js"></script>
    <script type="text/javascript" src="tools.js"></script>
    <script type="text/javascript">
        var salon_ok = true;
        var salon = -1;
        var chat = launch_chat();
        
     //   get_salon_url();
        
        
    </script>

</head>

<body onload="if(salon_ok) {setInterval('chat.update()', 1000)}">

    <div id="page-wrap-chat">
    
        <h2>Live Chat</h2>
        
        <p id="name-area"></p>
        
        <div id="chat-wrap"><div id="chat-area"></div></div>
        
        <form id="send-message-area">
            <textarea id="sendie" maxlength = '100' ></textarea>
        </form>
    
    </div>
    
        <center style="width:100%">
            <video style="width: 74%;margin-top: 5%;margin-left: 317px;max-height: 600px;" id="video" controls></video>
        </center>
    
    <script>
          $.ajax({
                type: "POST",
		url: "salons.php",
		data: {  
                    'salon': salon,
                    'mode':"read"
                },
		dataType: "json",
			
		success: function(data){
                    url = data["0"];  
                    if (url != null) {
                        launch_stream(url);
                    } else  {
                        if (salon != -1) {
                        $( "body" ).html("<link rel='stylesheet' href='style.css' type='text/css' />\n\
                                          <div id='refused'>\n\
                                                <p>Le salon selectionné n'existe pas ou n'est pas disponible</p>\n\
                                          </div>");
                        } else {
                            $( "body" ).html("<link rel='stylesheet' href='style.css' type='text/css' />\n\
                                          <div id='refused'>\n\
                                                <p>Aucun salon selectionné, veuillez le préciser dans l'URL</p>\n\
                                          </div>");
                        }
                        $( "body" ).css( "background", "black" );
                    }
                },
	    });
    </script>

</body>

</html>