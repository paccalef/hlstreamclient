<?php

if ($_POST['mode'] == 'read') {
    $answer = "";

    if (file_exists('salons.xml')) {
        if ($_POST['salon'] != -99) {
            $xml = simplexml_load_file('salons.xml');

            foreach($xml as $salon) {
                if ($_POST['salon'] == $salon->key) {
                    $answer = $salon->key;
                }

            }

            echo json_encode($answer);
        } else {
            $xml = simplexml_load_file('salons.xml');
            echo json_encode($xml);
        }
    } else {
        exit('Echec lors de l\'ouverture du fichier test.xml.');
    }
} else {
    if (file_exists('salons.xml')) {
        
        $all_ready = false;
        $rewrite = false;
        $xml = simplexml_load_file('salons.xml');
        
        foreach($xml as $salon) {
            if ($_POST['key'] == $salon->key) {
                    $all_ready = true;
                }
            if ($_POST['name'] == $salon->id) {
               $all_ready = true;
               $rewrite = true;
               $salon->key = $_POST['key'];
            }
        }
            
        if (!$all_ready) {
            $cs = $xml->addChild('salon','');
            $cs->addChild('id',$_POST['name']);
            $cs->addChild('key',$_POST['key']);
            $xml->saveXML('salons.xml');
            echo json_encode('Enregistrement effectué');
        } else {
            if ($rewrite) {
                $xml->saveXML('salons.xml');
                echo json_encode("Modification d'identifiant pris en compte pour le salon ".$_POST['name']);
            } else {
                echo json_encode('Déjà un enregistremement sous cet identifiant');
            }
        }
    }
}

?>